using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context;

public class AplicationService : DbContext
{
    public AplicationService(DbContextOptions<AplicationService> options) : base(options)
    {
        
    }
    public DbSet<Venda>? Vendas { get; set; }

    public DbSet<Pedido>? Pedidos { get; set; }

    public DbSet<Vendedor>? Vendedores { get; set; }
    public DbSet<Item>? Itens { get; set; }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Venda>()
        .HasOne(V => V.Vendedor)
        .WithMany();

        modelBuilder.Entity<Venda>()
        .HasMany(V => V.Pedidos)
        .WithOne(P => P.Venda);

        modelBuilder.Entity<Pedido>()
        .HasMany(P => P.Itens)
        .WithOne(I => I.Pedido);
        
    } 
}
