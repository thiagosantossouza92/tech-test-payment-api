using System.Net;
namespace tech_test_payment_api.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;
using tech_test_payment_api.Models.Enums;
using tech_test_payment_api.Context;
using System;

[ApiController]
[Route("Venda")]
public class VendaController : ControllerBase
{
    private readonly AplicationService _Context;

    public VendaController(AplicationService context)
    {
        _Context = context;
    }

    [HttpPost]
    public IActionResult NovoItem(Venda venda)
    {
        try 
        {
            venda.StatusVenda = StatusVenda.AguardandoPagamento;
            _Context.Vendas.Add(venda);
            _Context.SaveChanges();
            return Ok(venda);

        }catch(Exception e)
        {   
            return StatusCode(500, e.Message);
        }
    }
    [HttpGet]
    public IActionResult buscarPorId(int id)
    {
        var result =(from P in _Context.Pedidos
                    join  V in _Context.Vendas
                    on   P.Venda.Id equals V.Id
                    join Ven in _Context.Vendedores
                    on V.Vendedor.Id equals Ven.Id
                    where P.Venda.Id.Equals(id)
                    select new {
                        IdVenda = V.Id,
                        DataVenda = V.DataVenda,
                        StatusVenda = V.StatusVenda.ToString(),
                        Vendedor = Ven,
                        Itens = P.Itens,
                        TotalVenda = P.Itens.Select(x => x.Preco * x.Quantidade).Sum().ToString("C")
                    }).ToList();
        if(result != null && result.Count > 0)
            return Ok(result); 
        return BadRequest("Venda não encontrada!");
    }
    
    [HttpPut]
    public IActionResult AtualizarVenda(int id, StatusVenda trocaStatusVenda)
    {
        
        var vendaBd = _Context.Vendas.Find(id);
        var vendaBdStatus = vendaBd.StatusVenda;
        ValidaTrocaStatus validacao = new ValidaTrocaStatus();
        if(validacao.ValidaMudancaStatusVenda(vendaBd.StatusVenda, trocaStatusVenda)){
            vendaBd.StatusVenda = trocaStatusVenda;
            _Context.Vendas.Update(vendaBd);
            _Context.SaveChanges();
            return Ok($"Alteração de status da venda com identificação Id {vendaBd.Id} de {vendaBdStatus} para {trocaStatusVenda} realizada com sucesso!");
        }else{
            return BadRequest($"Mudança de status de {vendaBd.StatusVenda} para {trocaStatusVenda} não permitida, consulte a documentação para verificar as possíveis atualizações!");
        }
            
    }
}
