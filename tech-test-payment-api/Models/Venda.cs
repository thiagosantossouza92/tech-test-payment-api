namespace tech_test_payment_api.Models;
using tech_test_payment_api.Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

public class Venda
{
    public int Id {get; set;}
    public StatusVenda StatusVenda {get; set;}
    public DateTime DataVenda {get; set;}
    public Vendedor? Vendedor {get; set;}

    [Required]
    public ICollection<Pedido>? Pedidos { get; set; }

}
