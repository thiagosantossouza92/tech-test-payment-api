namespace tech_test_payment_api.Models;
using tech_test_payment_api.Models.Enums;

public class ValidaTrocaStatus
{
        public bool ValidaMudancaStatusVenda(StatusVenda status, StatusVenda MudarPara)
    {
        List<StatusVenda> operacoes = new List<StatusVenda>();
        switch(status){
            case StatusVenda.AguardandoPagamento: 
                operacoes.Clear();
                operacoes.Add(StatusVenda.Cancelada);
                operacoes.Add(StatusVenda.PagamentoAprovado);
                return operacoes.Contains(MudarPara);

            case StatusVenda.PagamentoAprovado:
                operacoes.Clear();
                operacoes.Add(StatusVenda.EnviadoParaTransportadora);
                operacoes.Add(StatusVenda.Cancelada);
                return operacoes.Contains(MudarPara);
            case StatusVenda.EnviadoParaTransportadora:
                operacoes.Clear();
                operacoes.Add(StatusVenda.Entregue);
                return operacoes.Contains(MudarPara);
        }
        return false;
    }
}
