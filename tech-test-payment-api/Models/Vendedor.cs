namespace tech_test_payment_api.Models;
public class Vendedor
{
    public int Id { get; set; }
    public String? Nome { get; set; }
    public String? Cpf { get; set; }
    public String? Telefone { get; set; }
    public String? Email { get; set; }

}