namespace tech_test_payment_api.Models.Enums;

public enum StatusVenda
{
    
    PagamentoAprovado = 1,
    EnviadoParaTransportadora = 2,
    Entregue = 3,
    Cancelada = 4,
    AguardandoPagamento = 5
}

