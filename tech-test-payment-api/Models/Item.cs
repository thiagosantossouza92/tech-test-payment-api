namespace tech_test_payment_api.Models;
using System.Text.Json.Serialization;
public class Item
{
    public int Id { get; set; } 

    [JsonIgnore]
    public Pedido? Pedido { get; set; } 
    public int Quantidade { get; set; }
    public double Preco { get; set; }
    public string? Descricao { get; set; }

}
