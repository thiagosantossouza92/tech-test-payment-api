namespace tech_test_payment_api.Models;
using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;

public class Pedido
{
    public int Id { get; set; }

    [JsonIgnore]
    public Venda? Venda { get; set; }

    [Required]
    public ICollection<Item>? Itens { get; set;}
}
